%% Importing, preprocessing and analyzing the data from uBlox GNSS receiver
% folder = "D:/LAB/AuticlaLetiste/samekacc/data/ublox/";
% folder = "D:\LAB\AuticlaLetiste\samekacc\analysis\matlab\TestDataFolder\";
folder = "D:\LAB\AuticlaLetiste\samekacc\analysis\matlab\Nov� slo�ka (2)\";

discard_invalid = false;   % discard unvalid data according to accLength value.
threshold = 0.05 ;          % discard threshold according to accLength value.
F = dir(folder+"*.csv");
F = natsortfiles({F.name});  % alphanumeric sorting function from file exchange https://www.mathworks.com/matlabcentral/fileexchange/47434-natural-order-filename-sort



%% Importing the data from CSV files
% The data is imported as tables

leader = F{end};

follower = F(1:end-1);


leader = importLeaderFile(folder+leader);

for i = 1:numel(follower)
    follower{i} = importCarFile(folder+follower{i});
end

%% Converting tables to timetables
% Since the first column contains date and time, the timetable format seems
% appropriate.

leader = table2timetable(leader);

for i = 1:numel(follower)
follower{i} = table2timetable(follower{i});
end

%% Get just the relevant columns from the tables
% The two time tables contain a wealth of columns but here we will only use
% one and two of them, respectively. 

leader = leader(:,'gSpeed');
for i = 1:numel(follower)
follower{i} = follower{i}(:,{'gSpeed','relPosLength','accLength','relPosValid'});
end

%% discard invalid data
if discard_invalid
for i = 1:numel(follower)
map = (follower{i}.accLength>threshold) | (double(follower{i}.relPosValid)==1) ;
follower{i}.gSpeed(map)= NaN;
follower{i}.relPosLength(map)= NaN;
end
end

% %% artificially delay data - only for test purposes, don't forget to comment out !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% for i = 1:numel(follower)
% follower{i}.datetime = follower{i}.datetime + (i-1)*(1/24/60/60*5);
% end



%% Plotting the original data
% Let's explore the data visually first.

% set new color order so we have enough unique colors in plot
co = distinguishable_colors(numel(follower)+1);
set(groot,'defaultAxesColorOrder',co)

leg = strings(numel(follower)+1,1);
leg(1) = "L";
for i = 1:numel(follower)
leg(i+1) = "C"+i;
end
close all
figure('units','normalized','outerposition',[0 0 1 1]);
ax1 =subplot(2,1,1)
plot(leader.datetime, leader.gSpeed*3.6)
hold on
for i = 1:numel(follower)
plot(follower{i}.datetime, follower{i}.gSpeed*3.6)
end
legend(leg)
ylabel('Speed (km/h)');
hold off
ax2 =subplot(2,1,2)
ax2.ColorOrderIndex = 2;
hold on
for i = 1:numel(follower)
plot(follower{i}.datetime, follower{i}.relPosLength)
end
xlabel('Time (hh:mm)')
ylabel('Distance (m)');
linkaxes([ax1,ax2],'x');
hold off

