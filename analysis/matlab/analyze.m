%% Importing, preprocessing and analyzing the data from uBlox GNSS receiver

%% Importing the data from CSV files
% The data is imported as tables

%leader = 'Leader_191119_201734.csv';
leader = 'Leader_191119_202843.csv';
%leader = 'Leader_191119_204103.csv';

% follower = {
%         'Car1_191119_195839.csv'
%     };

follower = {
        'Car1_191119_202840.csv'
    };

% follower = {
%         'Car1_191119_204048.csv'
%     };

leader = importLeaderFile(leader);

for i = 1:numel(follower)
    follower{i} = importCarFile(follower{i});
end

%% Converting tables to timetables
% Since the first column contains date and time, the timetable format seems
% appropriate.

leader = table2timetable(leader);
follower{1} = table2timetable(follower{1});

%% Get just the relevant columns from the tables
% The two time tables contain a wealth of columns but here we will only use
% one and two of them, respectively. 

leader = leader(:,'gSpeed');
follower = follower{1}(:,{'gSpeed','relPosLength'});

%% Plotting the original data
% Let's explore the data visually first.

figure(1)

subplot(2,1,1)
plot(leader.datetime, leader.gSpeed)
hold on
plot(follower.datetime, follower.gSpeed)
legend('Leader', 'Follower')
hold off
subplot(2,1,2)
plot(follower.datetime, follower.relPosLength)

%% Converting date and time to time, starting at 0
% The two data sets do not have identical datetime vectors. They have
% different starting times. Below we find the smaller of the two.

time_leader = timeofday(leader.datetime);
time_follower = timeofday(follower.datetime);

t0 = min(time_leader(1),time_follower(1));

% time_leader = time_leader - t0;
% time_follower = time_follower - t0;

%% Manually picking a subset (a range) from the data
% Here we manually pick some subset (a range or a time interval) from the full table.

t1 = datetime('2019-11-19 00:00:52','InputFormat','yyyy-MM-dd HH:mm:ss') + t0;
t2 = datetime('2019-11-19 00:02:49','InputFormat','yyyy-MM-dd HH:mm:ss') + t0;

ls = leader(timerange(t1,t2),:);
fs = follower(timerange(t1,t2),:);

%% Plotting the selected subset (range) from the data
% Let's plot the data for the selected time interval.

figure(2)

subplot(2,1,1)
plot(ls.datetime, ls.gSpeed)
hold on
plot(fs.datetime, fs.gSpeed)
legend('Leader', 'Follower')
hold off
subplot(2,1,2)
plot(fs.datetime, fs.relPosLength)

%% Checking if regular sampling
% For later analysis, it is crucial to have an equidistantly sampled data.
% Let's check the regularity of the datetime vector.

isregular(ls)
isregular(fs)

%%
% Obviously, the measurements are not sampled regularly.

dtl = diff(ls.datetime);
dtl.Format = 'hh:mm:ss.SSSS';
[min(dtl), max(dtl)]
median(dtl)

dtf = diff(fs.datetime);
dtf.Format = 'hh:mm:ss.SSSS';
[min(dtf), max(dtf)]
median(dtf)

%%
% In fact, this is not the only problem. Another one is that the data for
% the leader and the follower contain different number of samples.

size(ls,1)
size(fs,1)

%% Combined object (velocities of the leader and the follower)
% It may be very convenient to create a single timetable. First, we need to
% relabel te variables in the two partial timetables. 

ls.Properties.VariableNames = {'v0'};
ls.Properties.VariableDescriptions = {'Velocity of the leader vehicle'};
ls.Properties.VariableUnits = {'m/s'};

fs.Properties.VariableNames = {'v1','d01'};
fs.Properties.VariableDescriptions = {'Velocity of the follower','Distance between the follower and the leader'};
fs.Properties.VariableUnits = {'m/s','m'};

%%
% Now we aim at horizontal concatenation. Apparently, it will only work if
% both tables have an identical number of rows and the times are equal too.

%lfs = [ls fs];
dt = duration(0,0,0,100)

lfs = synchronize(ls,fs,'regular','linear','TimeStep',dt)
lfs.Properties.Description = 'Velocities of the leader and the follower in some window';

dt = diff(lfs.datetime);
dt.Format = 'hh:mm:ss.SSSS';
[min(dt), max(dt)]
median(dt)

%% SysID
u = lfs.v0
y = lfs.v1
Ts = 0.1
