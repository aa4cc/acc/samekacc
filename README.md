# samekacc

Experiments with a single car equipped with an adaptive cruise control (ACC) following a leader. The follower was Skoda Superb (2019), the leader was Renault Megane.

Three sensors:
*  IMU [ADIS16465](https://www.analog.com/en/products/adis16465.html#) with [ADIS16IMU4/PCBZ](https://www.analog.com/en/design-center/evaluation-hardware-and-software/evaluation-boards-kits/EVAL-ADIS16IMU4.html#eb-documentation) breakout board and [EVAL-ADIS2](https://www.analog.com/en/design-center/evaluation-hardware-and-software/evaluation-boards-kits/EVAL-ADIS2.html#eb-overview) evaluation kit.
*  GNSS [ZED-F9P module](https://www.u-blox.com/en/product/zed-f9p-module). In particular, breakout board [SparkFun GPS-RTK2 Board - ZED-F9P (Qwiic)](https://www.sparkfun.com/products/15136)
*  GoPro dashboard camera (including inertial sensors)

The data are stored on Github in LFS format. In order to be able to get the data using `git`, a [git-lfs extension](https://git-lfs.github.com/) must be installed.

For reading and processing the data from uBlox GNSS, [here is the link to the tables](https://www.u-blox.com/sites/default/files/u-blox_ZED-F9P_InterfaceDescription_%28UBX-18010854%29.pdf). During the export from the sensors the variables were scaled into basic units (m, m/s, ...).

# Description of csv data columns

| Column             | Desctription                                                                                              | Units | Example                    |
|--------------------|-----------------------------------------------------------------------------------------------------------|-------|----------------------------|
| datetime           | timestamp of sample                                                                                       |       | 2019-11-19 20:17:46.599704 |
| lon                | Longitude of position                                                                                     | °     | 14.2805147                 |
| lat                | Latitude of position                                                                                      | °     | 50.0306663                 |
| height             | Height above ellipsoid                                                                                    | m     | 386.846                    |
| hMSL               | Height above mean sea level                                                                               | m     | 342.312                    |
| hAcc               | Horizontal accuracy estimate                                                                              | m     | 0.389                      |
| vAcc               | Vertical accuracy estimate                                                                                | m     | 0.539                      |
| velN               | NED north velocity                                                                                        | m/s   | -0.005                     |
| velE               | NED east velocity                                                                                         | m/s   | 0.004                      |
| velD               | NED down velocity                                                                                         | m/s   | -0.027                     |
| gSpeed             | Ground Speed (2-D)                                                                                        | m/s   | 0.006                      |
| headMot            | Heading of motion (2-D)                                                                                   | °     | 117.50315                  |
| sAcc               | Speed accuracy estimate                                                                                   | m/s   | 0.165                      |
| headAcc            | Heading accuracy estimate (both motion and vehicle)                                                       | °     | 180.00000000000003         |
| refStationId       | Reference Station ID. Must be in the range 0..4095                                                        |       | 0                          |
| iTOW               | GPS time of week of the navigation epoch. See the description of iTOW for details.                        |       | 245884600                  |
| relPosN            | North component of relative position vector                                                               | m     | -2.5985                    |
| relPosE            | East component of relative position vector                                                                | m     | -3.0134                    |
| relPosD            | Down component of relative position vector                                                                | m     | -0.1183                    |
| relPosLength       | Length of the relative position vector                                                                    | m     | 3.9808                     |
| relPosHeading      | Heading of the relative position vector                                                                   | °     | 229.2                      |
| accN               | Accuracy of relative position North component                                                             | m     | 0.0167                     |
| accE               | Accuracy of relative position East component                                                              | m     | 0.0122                     |
| accD               | Accuracy of relative position Down component                                                              | m     | 0.0204                     |
| accLength          | Accuracy of length of the relative position vector                                                        | m     | 0.0143                     |
| accHeading         | Accuracy of heading of the relative position vector                                                       | °     | 0.2                        |
| gnssFixOK          | A valid fix (i.e within DOP & accuracy masks)                                                             |       | True                       |
| diffSoln           | differential corrections were applied                                                                     |       | True                       |
| relPosValid        | relative position components and accuracies are valid and, in moving base mode only, if baseline is valid |       | True                       |
| carrSoln           | Carrier phase range solution status                                                                       |       | True                       |
| isMoving           | the receiver is operating in moving base mode                                                             |       | False                      |
| refPosMiss         | extrapolated reference position was used to compute moving base solution this epoch                       |       | True                       |
| refObsMiss         | extrapolated reference observations were used to compute moving base solution this epoch                  |       | False                      |
| relPosHeadingValid | relPosHeading is valid                                                                                    |       | False                      |
| relPosNormalized   | the components of the relative position vector (including the high-precision parts) are normalized        |       | True                       |
